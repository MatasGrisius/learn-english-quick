# Learn english quick

### Technologies

Learn english quick uses number of technologies:
* Dotnet core 2.0
* ReactJs 15
* Entity framework

### Installation

***For frontend server:***
to console:
```
cd learn-english-quick/frontend
npm install
npm start
```

***For backend server:***
open project with Visual Studio
write `Update-Database` in Package Manager Console
then in console:
```
cd learn-english-quick/backend/backend
dotnet run
```

### Authors

* Matas Grišius - developing
* Aivaras Augustinas - developing
* Kristina Rudokaitė - documentation
* Margarita Kartavičiūtė - documentation
