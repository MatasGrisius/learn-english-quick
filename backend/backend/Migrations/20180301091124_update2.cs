﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace backend.Migrations
{
    public partial class update2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "words_count",
                table: "words");

            migrationBuilder.AddColumn<string>(
                name: "words_list",
                table: "words",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "words_list",
                table: "words");

            migrationBuilder.AddColumn<long>(
                name: "words_count",
                table: "words",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
