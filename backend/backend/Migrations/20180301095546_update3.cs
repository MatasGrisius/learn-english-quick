﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace backend.Migrations
{
    public partial class update3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "words_list",
                table: "words",
                newName: "words");

            migrationBuilder.RenameColumn(
                name: "list_name",
                table: "words",
                newName: "name");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "words",
                table: "words",
                newName: "words_list");

            migrationBuilder.RenameColumn(
                name: "name",
                table: "words",
                newName: "list_name");
        }
    }
}
