﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace backend.Migrations
{
    public partial class update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "words",
                table: "words",
                newName: "list_name");

            migrationBuilder.RenameColumn(
                name: "name",
                table: "words",
                newName: "description");

            migrationBuilder.AddColumn<long>(
                name: "words_count",
                table: "words",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "words_count",
                table: "words");

            migrationBuilder.RenameColumn(
                name: "list_name",
                table: "words",
                newName: "words");

            migrationBuilder.RenameColumn(
                name: "description",
                table: "words",
                newName: "name");
        }
    }
}
