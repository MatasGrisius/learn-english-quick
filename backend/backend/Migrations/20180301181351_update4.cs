﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace backend.Migrations
{
    public partial class update4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "url",
                table: "words",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_words_url",
                table: "words",
                column: "url",
                unique: true,
                filter: "[url] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_words_url",
                table: "words");

            migrationBuilder.AlterColumn<string>(
                name: "url",
                table: "words",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
