﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models
{
    public class Words
    {
        public int Id { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string words { get; set; }
    }
}
