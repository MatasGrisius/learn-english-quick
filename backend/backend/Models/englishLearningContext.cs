﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models
{
    public class englishLearningContext : DbContext
    {
        public englishLearningContext(DbContextOptions<englishLearningContext> options)
            : base(options)
        { }

        public DbSet<Words> words { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Words>()
                 .HasIndex(u => u.url)
                 .IsUnique();
        }
    }
}
