﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace backend.Controllers
{
    [Route("api/[controller]")]
    public class WordsController : Controller
    {
        englishLearningContext DbContext;

        public WordsController(englishLearningContext context)
        {
            DbContext = context;
        }
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(DbContext.words.ToArray());
        }
        // GET api/<controller>/url
        [HttpGet("{url}")]
        public IActionResult Get(string url)
        {
            if (url == null)
            {
                DbContext.words.ToArray();
            }
            Words wordsByUrl = DbContext.words.Where(b => b.url == url).FirstOrDefault();
            if (wordsByUrl == null) return NotFound("Žodžių rinkinio tokiu adresu nerasta");
            else return Ok(wordsByUrl);
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody] Words row)
        {
            if(row.words.Split("\n").Length>500)
            {
                return BadRequest("too much words");
            }
            if (row == null) return NoContent();
            if (DbContext.words.Where(b => b.url == row.url).Any())
                return BadRequest("Žodžių sarašas url:" + row.url + " jau yra sukurtas");
            DbContext.words.Add(row);
            DbContext.SaveChanges();
            return Ok(row);
        }
    }
}
