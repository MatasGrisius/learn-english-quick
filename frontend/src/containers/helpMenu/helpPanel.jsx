import React from 'react';
import { connect } from 'react-redux';
import { compareSymbols } from '../../utils/comparer';

class HelpPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hint: this.generateHint(this.props.correctWord),
      value: ''
    }
    setInterval(this.addLetterToBeginning, 1000);
    setInterval(this.addLetterRandom, 1000);
  }

  addLetterToBeginning = () => {
    if (this.props.help.showfrombeginning) {
      if (this.state.hint.filter(h => h.symbol === "_").length > 0) {
        let index = this.state.hint.find(h => h.symbol === "_").id;
        this.setState({
          hint: [
            ...this.state.hint.slice(0,index),
            {
              ...this.state.hint[index],
              symbol: this.state.hint[index].letter
            },
            ...this.state.hint.slice(index+1)
          ]
        })
      }
    }
  }

  addLetterRandom = () => {
    if (this.props.help.showrandom) {
      if (this.state.hint.filter(h => h.symbol === "_").length > 0) {
        let items = this.state.hint.filter(h => h.symbol === "_");
        let index = items[Math.floor(Math.random()*items.length)].id;
        this.setState({
          hint: [
            ...this.state.hint.slice(0,index),
            {
              ...this.state.hint[index],
              symbol: this.state.hint[index].letter
            },
            ...this.state.hint.slice(index+1)
          ]
        })
      }
    }
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.correctord !== nextProps.correctWord) {
      this.setState({
        hint: this.generateHint(nextProps.correctWord)
      })
    }
  }

  generateHint = (correctWord) => {  
    return correctWord.split('').map((letter, i) => {
      let object = {
        letter,
        symbol: "_",
        id: i
      }
      if (this.props.help.showSymbols && object.letter.toUpperCase() === object.letter.toLowerCase()) {
        object.symbol = object.letter;
      }
      return object;
    });
  }

  render() {
    return (
      <div>
        <input className={"game_input" + (this.props.help.redgreen===true ? (compareSymbols(this.props.correctWord, this.state.value) ? ' greentext' : ' redtext') : '')} 
          onChange={(event) => {
            if (this.props.onChange) this.props.onChange(event);
            this.setState({
              value: event.target.value
            })
          }}
          onKeyDown={this.props.onSubmit}
        />
        {this.props.help.showanswer ? 
          <div className="help-tip">
            <p className="helpText">{this.props.correctWord}</p>
          </div>
        :
        <div/>
        }
        <br />
        {this.props.help.lettercount ? 
          <div className="center helpText"> 
            {this.state.hint.map((letter) => <span key={letter.id}>{letter.symbol} </span>)}
          </div> 
        :
          <div/>
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  help: state.help
})

export default connect(
  mapStateToProps
)(HelpPanel)