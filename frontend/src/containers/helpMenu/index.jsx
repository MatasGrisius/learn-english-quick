import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
  toggleCaseSensative,
  toggleRedGreen,
  toggleLetterCount,
  toggleShowFromBeginning,
  toggleShowRandom,
  toggleShowAnswer,
  toggleShowSymbols
} from '../../modules/help'

class HelpMenu extends React.Component {
  render() {
    return (
      <div>
        <h1>Pagalbų bei nustatymų meniu</h1>

        <input type="checkbox" checked={this.props.help.casesensative} onChange={this.props.toggleCaseSensative}/>
        <label>Skirti mažąsias/didžiąsias raides</label>
        <br />

        <input type="checkbox" checked={this.props.help.redgreen} onChange={this.props.toggleRedGreen}/>
        <label>Indikuoti raudona/žalia spalvomis žodžio vedimo metu</label>
        <br />

        <input type="checkbox" checked={this.props.help.lettercount} onChange={this.props.toggleLetterCount}/>
        <label>Parodyti raidžių kiekį žodyje</label>
        <br />

        <input type="checkbox" disabled={!this.props.help.lettercount} checked={this.props.help.showsymbols} onChange={this.props.toggleShowSymbols}/>
        <label>Rodant raidžių kiekį simbolius rodyti iškart</label>
        {!this.props.help.lettercount ? <label>(privalo būti įjungta raidžių kiekio pagalba)</label> : <span />}
        <br />

        <input type="checkbox" disabled={!this.props.help.lettercount} checked={this.props.help.showfrombeginning} onChange={this.props.toggleShowFromBeginning}/>
        <label>Kas kelios sekundės parodyti po naują atsitiktinę raidę</label>
        {!this.props.help.lettercount ? <label>(privalo būti įjungta raidžių kiekio pagalba)</label> : <span />}
        <br />

        <input type="checkbox" disabled={!this.props.help.lettercount} checked={this.props.help.showrandom} onChange={this.props.toggleShowRandom}/>
        <label>Kas kelios sekundės parodyti po naują atsitiktinę raidę</label>
        {!this.props.help.lettercount ? <label>(privalo būti įjungta raidžių kiekio pagalba)</label> : <span />}
        <br />

        <input type="checkbox" checked={this.props.help.showanswer} onChange={this.props.toggleShowAnswer}/>
        <label>Paprašius parodyti atsakymą</label>
        <br />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  help: state.help
})

const mapDispatchToProps = dispatch => bindActionCreators({
  toggleCaseSensative,
  toggleRedGreen,
  toggleLetterCount,
  toggleShowFromBeginning,
  toggleShowRandom,
  toggleShowAnswer,
  toggleShowSymbols
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HelpMenu)