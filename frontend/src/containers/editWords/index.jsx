import React from 'react'
import { textToWords, wordsToText } from "../../utils/wordsConverter";
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
    setWords,
} from '../../modules/words'

class EditWords extends React.Component {
    
    handleChange = (event) => {
        this.props.setWords(textToWords(event.target.value));
    }

    render() {
      return (
          <div>
            <h1> Žodžių redagavimas </h1>
            <p>Kiekviena eilutė turi būti sudaryta iš dviejų žodžių atskirtų kabliataškiu ;</p>
            <p>Pirmas žodis yra žodžio reikšmė, o antras - tai ką norite išmokti :)</p>
            <p>Pavyzdys: </p>
            <textarea rows="3" cols="60" readOnly disabled value="Ride;Joti&#10;Laugh;Juoktis&#10;Funny;Juokingas"/>
            <div>
                <hr />
                <textarea rows="25" cols="60" defaultValue={wordsToText(this.props.words)} onChange={this.handleChange}/>
            </div>
          </div>
      )
    }
}

const mapStateToProps = state => ({
    words: state.words,
  })
  
const mapDispatchToProps = dispatch => bindActionCreators({
    setWords,
  }, dispatch)
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(EditWords)