import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
  saveWordlist
} from '../../modules/wordlists'
import { wordsToText } from '../../utils/wordsConverter';

class SaveWordlist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      wordlist: {

      }
    }
  }

  onInput = (event, name) => {
    this.setState({
      wordlist: {
        ...this.state.wordlist,
        [name]: event.target.value

      }
    })
  }

  onSubmit = () => {
    this.props.saveWordlist({
      ...this.state.wordlist,
      words: wordsToText(this.props.words)
    })
  }

  render() {
    return (
      <div>
        <h1>Įkelti savo žodžių sąrašą į interneto duomenų bazę</h1>
        {
          this.props.words.length > 0 ? 
          <div>
            <table>
              <tbody>
                <tr>
                  <th>Žodis</th>
                  <th>Reiškmė</th>
                </tr>
                {this.props.words.map((wl,i) => 
                <tr key={i}>
                  <td>
                    {wl.first}
                  </td>
                  <td>
                    {wl.second}
                  </td>
                </tr>
                )}
              </tbody>
            </table>
            <input placeholder="Sąrašo pavadinimas" onChange={(e) => this.onInput(e,"name")} />
            <input placeholder="Aprašymas" onChange={(e) => this.onInput(e,"description")}/>
            <input placeholder="url adresas" onChange={(e) => this.onInput(e,"url")}/>
            <button onClick={this.onSubmit}>Išsaugoti</button>
          </div>
          : 
          <p>Jūs neturite nei vieno žodžio sąraše :(</p>
        }
    </div>
    )
  }
}

const mapStateToProps = state => ({
  words: state.words
})

const mapDispatchToProps = dispatch => bindActionCreators({
  saveWordlist
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SaveWordlist)