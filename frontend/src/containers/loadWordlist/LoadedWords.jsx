import React from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {downloadWordlistbyUrl} from '../../modules/wordlists'; 
import {
  setWords
} from '../../modules/words'
import { textToWords } from '../../utils/wordsConverter';

class LoadedWords extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentWillMount = () => {
    this.props.downloadWordlistbyUrl(this.props.match.params.url);
  }

  componentWillReceiveProps(nextProps){
    if (this.props.wordlists.loadedWordlist===false && nextProps.wordlists.loadedWordlist === true && nextProps.wordlists.error === false){
      this.props.setWords(textToWords(nextProps.wordlists.wordlist.words));
    }
  }

  render() {
    if (!this.props.wordlists.loadedWordlist) {
      return <h3>Kraunama...</h3>
    } 
    else
    {
      if (this.props.wordlists.error) {
        return <h3>Klaida: Žodžiai nurodytu adresu nerasti.</h3>
      } 
      else 
      {
        let words = textToWords(this.props.wordlists.wordlist.words);
        return (
          <div>
            <h1>Įkelti žodžiai</h1>
            <h3>{this.props.wordlists.wordlist.name}</h3>
            <h4>{this.props.wordlists.wordlist.description}</h4>
              <table>
                <tbody>
                  <tr>
                    <th>Žodis</th>
                    <th>Reiškmė</th>
                  </tr>
                  {words.map((w,i) => 
                  <tr key={i}>
                    <td>
                      {w.first}
                    </td>
                    <td>
                      {w.second}
                    </td>
                  </tr>
                  )}
                </tbody>
              </table>
          </div>
        )
      }
    }
  }
}

const mapStateToProps = state => ({
  wordlists: state.wordlists
})

const mapDispatchToProps = dispatch => bindActionCreators({
  downloadWordlistbyUrl,
  setWords
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoadedWords)