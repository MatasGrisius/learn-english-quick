import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from "react-router-dom";
import {
  fetchWordlists
} from '../../modules/wordlists'
import {
  setWords
} from '../../modules/words'

class LoadWordlist extends React.Component {
  
  componentWillMount = () => {
    this.props.fetchWordlists();
  }

  onClick = (id) => {
    this.props.history.push("/loaded/" + this.props.wordlists.wordlists[id].url);
  }

  render() {
    if (!this.props.wordlists.loadedWordlists) {
      return <h3>Kraunama...</h3>;
    } else if(this.props.wordlists.error) {
      return <h3>Įvyko klaida</h3>;
    }
    return (
      <div>
        <h1>Žodžių sąrašai iš interneto duomenų bazės</h1>
        {
          this.props.wordlists.wordlists.length > 0 ? 
          <table>
            <tbody>
              <tr>
                <th>url adresas</th>
                <th>sąrašo pavadinimas</th>
                <th>aprašymas</th>
                <th>žodžių kiekis</th>
                <th></th>
              </tr>
              {this.props.wordlists.wordlists.map((wl,i) => 
              <tr key={i}>
                <td>
                  {wl.url}
                </td>
                <td>
                  {wl.name}
                </td>
                <td>
                  {wl.description}
                </td>
                <td>
                  {wl.words.length}
                </td>
                <td>
                  <input type="button" value="Parsiųsti" onClick={() => this.onClick(i)}/>
                </td>
              </tr>
              )}
            </tbody>
          </table>
          : 
          <p>Žodžių sąrašų nerasta :(</p>
        }
    </div>
    )
  }
}

const mapStateToProps = state => ({
  wordlists: state.wordlists
})

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchWordlists,
  setWords
}, dispatch)

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(LoadWordlist))