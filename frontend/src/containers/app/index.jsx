import React from 'react';
import { Route, NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Home from '../home';
import About from '../about';
import EditWords from '../editWords';
import LoadWordlist from '../loadWordlist';
import LoadedWords from '../loadWordlist/LoadedWords';
import SaveWordlist from '../saveWordlist';
import HelpMenu from '../helpMenu';
import { setWords } from '../../modules/words';

class App extends React.Component {
  componentDidMount = () => {
    if (this.props.words.length === 0 && window.localStorage.getItem('words')) {
      this.props.setWords(JSON.parse(window.localStorage.getItem('words')));
    }
  };

  render = () => (
    <div>
      <header>
        <ul className="navigation_container">
          <NavLink activeClassName="navigation_item_active" exact className="navigation_item" to="/">
            Žaidimas
          </NavLink>
          <NavLink activeClassName="navigation_item_active" className="navigation_item" to="/about-us">
            Apie
          </NavLink>
          <NavLink activeClassName="navigation_item_active" className="navigation_item" to="/edit-words">
            Redaguoti žodžius
          </NavLink>
          <NavLink activeClassName="navigation_item_active" className="navigation_item" to="/load-wordlist">
            Įkelti žodžius
          </NavLink>
          <NavLink activeClassName="navigation_item_active" className="navigation_item" to="/save-wordlist">
            Išsaugoti žodžius
          </NavLink>
          <NavLink activeClassName="navigation_item_active" className="navigation_item" to="/help-menu">
            Pagalbų meniu
          </NavLink>
        </ul>
      </header>

      <main>
        <Route exact path="/" component={Home} />
        <Route exact path="/about-us" component={About} />
        <Route exact path="/edit-words" component={EditWords} />
        <Route exact path="/load-wordlist" component={LoadWordlist} />
        <Route exact path="/save-wordlist" component={SaveWordlist} />
        <Route exact path="/help-menu" component={HelpMenu} />
        <Route path="/loaded/:url" component={LoadedWords} />
      </main>
    </div>
  );
}

const mapStateToProps = state => ({
  words: state.words,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setWords,
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
);
