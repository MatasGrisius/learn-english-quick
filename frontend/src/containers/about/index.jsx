import React from 'react'

export default () => (
  <div>
    <h1>Apie programą</h1>
    <p>"Quick Way to Learn" - internetinė programa, padėsianti greičiau išmokti užsienio kalbos žodžius ar prioritetinius klausimus atsiskaitymui. Programa pritaikyta įsivesti norimą išmokti žodyną ar temas, o vėliau iš įvedimo formuojamas klausimynas. Naudotojas turi atsakyti kaip rašosi žodis norima išmokti kalba, pagal lietuvišką (arba bet kurį kitą) apibrėžimą arba tiesioginį vertimą. O jei naudojama su klausimais, kurie neturi konkretaus atsakymo - vartotojas pats turi nuspręsti ar atsakė teisingai. Kadangi programos naudojamus žodžius ar klausimus įkelia vartotojas, ši programa gali būti pritaikyta mokytis bet kurios kalbos, atitinkančios reikalavimus. Programoje yra keli režimai:</p>
    <p>1.	Atsitiktiniu būdu išrinkto žodžio paklausimas. Kai įjungta teisingos raidės indikatoriaus pagalba, jei vedama teisinga žodžio raidžių seka - vedamas žodis nuspalvinamas žaliai, jei ne - raudonai </p>
    <p>2.	Testo režimas, kurio metu neveikia pagalbos ir visi žodžiai paklausiami po vieną kartą. Pačiame gale nurodomas pažymys (pagal tai, kiek klausimų atsakyta) ir išvedamos klaidos;</p>
    <p>3.	Mokymosi režimas, kai klausinėjama iš riboto sąrašo, po truputį jį didinant;</p>
    <p>4.	Klausinėjimas be atsakymų, kai atsakymo įvesti nereikia ir pats naudotojas pasirenka ar atsakė teisingai. Patogus naudoti ruošiantis atsiskaitymams, kai turimi prioritetiniai klausimai, kurie neturi tikslaus ir vienintelio atsakymo;</p>
    <p>Šis įrankis informaciją priima įkėlus tekstinį failą, įvedus informaciją į tekstinį lauką arba pasirinkus iš esamos duomenų bazės. Duomenų bazėje iškart galite rasti 100 anglų kalbos žodžių išbandyti savo jėgas. Savo įvestą informaciją galima išsaugoti į tinklalapio duomenų bazę ir tokiu būdu nusiųsti nuorodą kitam naudotojui į kitą įrenginį, kuriame nustatyti klausimai automatiškai užsikraus. Tokiu būdu galima dalintis savo klausimais su kitais. </p>
    <p>© Karma inžinerijos projektas</p>
    <p>Kauno Technologijos Univeristetas</p>

  </div>
)