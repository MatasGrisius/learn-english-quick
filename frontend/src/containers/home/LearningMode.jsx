import React from 'react';
import HelpPanel from '../helpMenu/helpPanel';
import { compare } from '../../utils/comparer';

class LearningMode extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gameFinished: false,
      words: [],
      currentWord: {}
    }
    this.lastLevel = false;
  }

  onSubmit = (event) => {
    if (event.keyCode === 13) {
      if (compare(event.target.value,this.state.currentWord.first)) {
        let currentWordId = this.state.words.indexOf(this.state.currentWord);
        this.setState({
          words: this.state.words.filter((_, i) => i !== currentWordId)
        },
        this.getNewWord)
      } else {
        this.getNewWord();
      }
      event.target.value = "";
    }
  }

  endGame = () => {
    this.setState({gameFinished: true});
  }

  componentWillMount() {
    this.generateWords(10);
  }

  generateWords(n) {
    if (n >= this.props.words.length) {
      n = this.props.words.length;
      this.lastLevel = true;
    }
    this.setState({
      words: this.props.words.slice(0,n),
      wordsCount: n
    },
    this.getNewWord)
  }

  getNewWord() {
    if (this.state.words.length === 0) {
      if (this.lastLevel) {
        this.endGame();
        return;
      }
      this.generateWords(this.state.wordsCount+10);
      return;
    }
    let newWordId = Math.floor(Math.random() * this.state.words.length);
    this.setState({
      currentWord: this.state.words[newWordId]
    })
  }

  render() {
    if (this.state.gameFinished) {
      return (
        <div>
          <h1> Mokymosi režimas: rezultatai </h1>
          {this.lastLevel && this.state.words.length === 0 ? <h3 className="greentext"> Išmokote visus {this.props.words.length} žodžius</h3> : 
              this.state.wordsCount - this.state.wordsCount%10 - 10 > 0 ? <h3 className="yellowtext">Nutraukėte žaidimą, tačiau spėjote išmokti {this.state.wordsCount - this.state.wordsCount%10 - 10} žodžių iš {this.props.words.length}</h3> : <h3 className="redtext">Nutraukėte žaidimą</h3>}
          <button className="end_game_button" onClick={this.props.endGame}>Baigti žaidimą</button>
        </div>
      )
    }
    return (
      <div className="center">
        <h1 className="left"> Mokymosi režimas </h1>
        <h3 className="zodziuKiekis"> Žodžių kiekis: {this.state.wordsCount}/{this.props.words.length} </h3>
        <h3 className="zodziuKiekis"> Iki {this.lastLevel ? " mokymosi pabaigos " : " žodžių papildimo "}liko: {this.state.words.length} žodžiai</h3>
        <h2 className="game_word">{this.state.currentWord.second}</h2>
        {this.state.currentWord.first ? 
        <HelpPanel
          onSubmit={this.onSubmit}
          correctWord={this.state.currentWord.first}
        /> :
        <div />}
        <br/> 
        <button className="end_game_button" onClick={() => {this.endGame(1)}}>Baigti žaidimą</button>
      </div>
    )
  }
}

export default LearningMode;