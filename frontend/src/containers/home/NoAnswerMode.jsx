import React from 'react'

class NoAnswerMode extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      zodzioId: -1,
      gameFinished: false,
      wordsCountCorrect: -1,
      wordsCountIncorrect: 0
    }
    this.badWords = [];
  }

  endGame = () => {
    this.setState({gameFinished: true});
  }

  componentWillMount() {
    this.getNewWord(true);
  }

  getNewWord(correct) {
    let newWordId = this.state.zodzioId;
    while (newWordId === this.state.zodzioId) {
      newWordId = Math.floor(Math.random() * this.props.words.length);
    }
    let nameOfvar = correct ? "wordsCountCorrect" : "wordsCountIncorrect";
    this.setState({
      zodzioId: newWordId,
      [nameOfvar]: this.state[nameOfvar] + 1
    })
  }

  render() {
    if (this.state.gameFinished) {
      return (
        <div>
          <h1> Klausinėjimas be atsakymų: rezultatai </h1>
          <h2 className="greentext"> Atsakyti klausimai: {this.state.wordsCountCorrect} </h2>
          <h2 className="redtext"> Neatsakyti klausimai: {this.state.wordsCountIncorrect} </h2>
          <p> Būtinai pasikartokite šiuos klausimus:</p>
          {this.badWords.map((word, i) => 
            <p key={i}>{word}</p>
          )}
          <button className="end_game_button" onClick={this.props.endGame}>Baigti žaidimą</button>
        </div>
      )
    }
    return (
      <div className="center">
        <h1 className="left"> Klausinėjimas be atsakymų </h1>
        <h3 className="zodziuKiekis"> Paklausta klausimų: {this.state.wordsCountCorrect + this.state.wordsCountIncorrect} </h3>
        <h2 className="game_word">{this.props.words[this.state.zodzioId].second}</h2>
        <button className="answer_button green"onClick={() => this.getNewWord(true)}>Žinau atsakymą</button>
        <button className="answer_button red" onClick={() => {this.badWords.push(this.props.words[this.state.zodzioId].second); this.getNewWord(false)}}>Nežinau atsakymo</button>
        <br/> 
        <button className="end_game_button" onClick={this.endGame}>Baigti žaidimą</button>
      </div>
    )
  }
}

export default NoAnswerMode;