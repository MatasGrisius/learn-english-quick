import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { GAMETYPES } from '../../consts.js';
import RandomMode from './RandomMode';
import TestMode from './TestMode';
import NoAnswerMode from './NoAnswerMode';
import LearningMode from './LearningMode';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gameStarted: false,
      gameMode: null,
    }
  }

  startGame = (gameMode) => {
    if (this.props.words.length >= 2) {
      this.setState(Object.assign(this.state, {
        gameStarted: true,
        gameMode,
      }))
    } else {
      alert("Jums reikia nors 2 zodziu!");
    }
  }

  render() {
    if (this.state.gameStarted) {
      switch(this.state.gameMode) {
        case GAMETYPES.RANDOM: 
          return <RandomMode
            words={this.props.words}
            endGame={() => this.setState({gameStarted:false})}
          />
        case GAMETYPES.TEST: 
          return <TestMode
            words={this.props.words}
            endGame={() => this.setState({gameStarted:false})}
          />
        case GAMETYPES.NOANSWER: 
          return <NoAnswerMode
            words={this.props.words}
            endGame={() => this.setState({gameStarted:false})}
          />
        case GAMETYPES.LEARNING: 
          return <LearningMode
            words={this.props.words}
            endGame={() => this.setState({gameStarted:false})}
          />
        default:
          return <div/>
      }
    }
    return (
      <div>
        <h1> Pasirinkite žaidimo režimą </h1>
        <h3 className="zodziuKiekis"> Žodžių kiekis: {this.props.words.length} </h3>
        <div className="game_mode">
          <h3>Klausinėti atsitiktine tvarka</h3>
          <ul className="game_mode_rules">
            <li>
              Žodžiai pasirenkami atsitiktinai
            </li>
            <li>
              Galima naudotis pagalbomis
            </li>
          </ul>
          <button className="game_mode_button" onClick={() => this.startGame(GAMETYPES.RANDOM)} >Žaisti</button>
        </div>
        <div className="game_mode">
          <h3>Mokymosi režimas</h3>
          <ul className="game_mode_rules">
            <li>
              Žodžiai klausinėjami iš riboto sąrašo, po truputį jį didinant
            </li>
            <li>
              Galima naudotis pagalbomis
            </li>
          </ul>
          <button className="game_mode_button" onClick={() => this.startGame(GAMETYPES.LEARNING)}>Žaisti</button>
        </div>
        <div className="game_mode">
          <h3>Testo režimas</h3>
          <ul className="game_mode_rules">
            <li>
              Visi žodžiai po vieną kartą
            </li>
            <li>
              Jokių pagalbų
            </li>
            <li>
              Pabaigoje parodomas pažymys
            </li>
          </ul>
          <button className="game_mode_button" onClick={() => this.startGame(GAMETYPES.TEST)}>Žaisti</button>
        </div>
        <div className="game_mode">
          <h3>Klausinėti be atsakymų</h3>
          <ul className="game_mode_rules">
            <li>
              Žodžiai klausiami atsitiktine tvarka nereikalaujant atsakymo
            </li>
            <li>
              Laisvai pasirenkame, kada klausti kito klausimo
            </li>
          </ul>
          <button className="game_mode_button" onClick={() => this.startGame(GAMETYPES.NOANSWER)}>Žaisti</button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  words: state.words
})

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)