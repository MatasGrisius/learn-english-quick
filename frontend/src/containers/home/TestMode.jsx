import React from 'react';
import { compare } from '../../utils/comparer';

class TestMode extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gameFinished: false,
      words: [...this.props.words],
      currentWord: "",
      penalty: 0
    }
    this.badWords = []
  }

  onSubmit = (event) => {
    if (event.keyCode === 13) {
      if (!compare(event.target.value,this.state.currentWord.first)) {
        this.badWords.push({
          correct: this.state.currentWord,
          bad: event.target.value
        });
      }
      this.getNewWord();
      event.target.value = "";
    }
  }

  endGame = (penalty) => {
    this.setState({gameFinished: true, penalty});
  }

  componentWillMount() {
    this.getNewWord();
  }

  getNewWord() {
    if (this.state.words.length === 0) {
      this.endGame(0);
      return;
    }
    let newWordId = Math.floor(Math.random() * this.state.words.length);
    this.setState({
      currentWord: this.state.words[newWordId],
      words: this.state.words.filter((_, i) => i !== newWordId)
    })
  }

  render() {
    if (this.state.gameFinished) {
      return (
        <div>
          <h1> Testo režimas: rezultatai </h1>
          <h2> Gerai atsakytų žodžių: {this.props.words.length - this.state.words.length - this.badWords.length - this.state.penalty} </h2>
          <h2> Blogai atsakytų žodžių: {this.badWords.length} </h2>
          {this.badWords.length > 0 ? <p> Klaidos: </p> : <div/>}
          {this.badWords.map((word, i) => 
            <p key={i}>Turėjo būti: {word.correct.first}. Jūsų atsakymas: {word.bad}</p>
          )}
          {this.state.penalty > 0 ? <p> Pastaba: Yra neatsakytų žodžių </p> : <div/>}
          <h2> Pažymys: </h2>
          <h1 className="test_mark">{((this.props.words.length - this.state.words.length - this.badWords.length - this.state.penalty)*10 / this.props.words.length).toFixed(1)}/10</h1>
          <button className="end_game_button" onClick={this.props.endGame}>Baigti žaidimą</button>
        </div>
      )
    }
    return (
      <div className="center">
        <h1 className="left"> Testo režimas </h1>
        <h3 className="zodziuKiekis"> Liko žodžių: {this.state.words.length} </h3>
        <h2 className="game_word">{this.state.currentWord.second}</h2>
        <input className="game_input" onKeyDown={this.onSubmit}/>
        <br/> 
        <button className="end_game_button" onClick={() => {this.endGame(1)}}>Baigti žaidimą</button>
      </div>
    )
  }
}

export default TestMode;