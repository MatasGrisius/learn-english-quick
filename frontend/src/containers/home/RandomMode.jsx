import React from 'react';
import HelpPanel from '../helpMenu/helpPanel';
import { compare } from '../../utils/comparer';


class RandomMode extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      zodzioId: -1,
      gameFinished: false,
      wordsCount: -1
    }
  }

  onInput = (event) => {
    if (compare(event.target.value,this.props.words[this.state.zodzioId].first)) {
      this.getNewWord();
      event.target.value = "";
    }
  }

  endGame = () => {
    this.setState({gameFinished: true});
  }

  componentWillMount() {
    this.getNewWord();
  }

  getNewWord() {
    let newWordId = this.state.zodzioId;
    while (newWordId === this.state.zodzioId) {
      newWordId = Math.floor(Math.random() * this.props.words.length);
    }
    this.setState({
      zodzioId: newWordId,
      wordsCount: this.state.wordsCount + 1
    })
  }

  render() {
    if (this.state.gameFinished) {
      return (
        <div>
          <h1> Atsitiktinis klausinėjimas: rezultatai </h1>
          <h2> Atsakytų žodžių: {this.state.wordsCount} </h2>
          <button className="end_game_button" onClick={this.props.endGame}>Baigti žaidimą</button>
        </div>
      )
    }
    return (
      <div className="center">
        <h1 className="left"> Atsitiktinis klausinėjimas </h1>
        <h3 className="zodziuKiekis"> Atsakytų žodžių: {this.state.wordsCount} </h3>
        <h2 className="game_word">{this.props.words[this.state.zodzioId].second}</h2>
        <HelpPanel
          onChange={this.onInput}
          correctWord={this.props.words[this.state.zodzioId].first}
        />
        <br/> 
        <button className="end_game_button" onClick={this.endGame}>Baigti žaidimą</button>
      </div>
    )
  }
}

export default RandomMode;