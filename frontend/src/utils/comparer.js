import store from '../store.js';

export function compare(string1, string2) {
    if (store.getState().help.casesensative===true) {
        return string1 === string2;
    }
    else {
        return string1.toUpperCase() === string2.toUpperCase();
    }
}

export function compareSymbols(string1, string2) {
    let min = Math.min(string1.length, string2.length);
    string1 = string1.substring(0,min);
    string2 = string2.substring(0,min);
    if (store.getState().help.casesensative===true) {
        return string1 === string2;
    }
    else {
        return string1.toUpperCase() === string2.toUpperCase();
    }
}