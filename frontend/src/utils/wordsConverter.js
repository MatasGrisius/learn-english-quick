export function textToWords(text){
    let words = [];
    let lines = text.split('\n');
    lines.forEach(line => {
        var first = line.split(';')[0];
        var second = line.split(';')[1];
        if (first && second) {
            words.push( {
                first,
                second
            })
        }
    });
    return words;
}

export function wordsToText(words) {
    if (! words) {
        return "";
    }
    return words.reduce((text, w) => `${text}${w.first};${w.second}\n`,"")
}