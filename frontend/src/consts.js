export const GAMETYPES = {
    RANDOM: 0,
    TEST: 1,
    LEARNING: 2,
    NOANSWER: 3
};