export const UPLOAD_NEW_WORDS_RECEIVED = 'words/UPLOAD_NEW_WORDS_RECEIVED';

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case UPLOAD_NEW_WORDS_RECEIVED:
      return [...action.words];

    default:
      return state;
  }
};

export const setWords = words => {
  window.localStorage.setItem('words', JSON.stringify(words));
  return dispatch => {
    dispatch({
      type: UPLOAD_NEW_WORDS_RECEIVED,
      words,
    });
  };
};
