export const TOGGLE_CASESENSATIVE = 'help/TOGGLE_CASESENSATIVE'
export const TOGGLE_REDGREEN = 'help/TOGGLE_REDGREEN'
export const TOGGLE_LETTERCOUNT = 'help/TOGGLE_LETTERCOUNT'
export const TOGGLE_SHOWANSWER = 'help/TOGGLE_SHOWANSWER'
export const TOGGLE_SHOWFROMBEGINNING = 'help/TOGGLE_SHOWFROMBEGINNING'
export const TOGGLE_SHOWRANDOM = 'help/TOGGLE_SHOWRANDOM'
export const TOGGLE_SHOWSYMBOLS = 'help/TOGGLE_SHOWSYMBOLS'

const initialState = {
  casesensative: true,
  redgreen: false,
  lettercount: false,
  showsymbols: false,
  showanswer: false,
  showfrombeginning: false,
  showrandom: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_CASESENSATIVE:
      return {
        ...state,
        casesensative: !state.casesensative
      }
    case TOGGLE_REDGREEN:
      return {
        ...state,
        redgreen: !state.redgreen
      }
    case TOGGLE_LETTERCOUNT:
      let obj = state.lettercount ? 
        {
          showfrombeginning: false,
          showrandom: false,
          showsymbols: false
        } : 
        {}
      return {
        ...state, 
        lettercount: !state.lettercount,
        ...obj
      }
    case TOGGLE_SHOWANSWER:
      return {
        ...state,
        showanswer: !state.showanswer
      }
    case TOGGLE_SHOWFROMBEGINNING:
      return {
        ...state,
        showfrombeginning: !state.showfrombeginning
      }
    case TOGGLE_SHOWRANDOM:
      return {
        ...state,
        showrandom: !state.showrandom
      }
    case TOGGLE_SHOWSYMBOLS:
      return {
        ...state,
        showsymbols: !state.showsymbols
      }
    default:
      return state
  }
}

export const toggleCaseSensative = () => 
dispatch => {
  dispatch({
    type: TOGGLE_CASESENSATIVE
  })
}

export const toggleRedGreen = () => 
dispatch => {
  dispatch({
    type: TOGGLE_REDGREEN
  })
}

export const toggleLetterCount = () => 
dispatch => {
  dispatch({
    type: TOGGLE_LETTERCOUNT
  })
}

export const toggleShowAnswer = () => 
dispatch => {
  dispatch({
    type: TOGGLE_SHOWANSWER
  })
}

export const toggleShowFromBeginning = () => 
dispatch => {
  dispatch({
    type: TOGGLE_SHOWFROMBEGINNING
  })
}

export const toggleShowRandom = () => 
dispatch => {
  dispatch({
    type: TOGGLE_SHOWRANDOM
  })
}

export const toggleShowSymbols = () => 
dispatch => {
  dispatch({
    type: TOGGLE_SHOWSYMBOLS
  })
}
