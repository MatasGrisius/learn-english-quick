import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import words from './words'
import wordlists from './wordlists'
import help from './help'

export default combineReducers({
  routing: routerReducer,
  words,
  wordlists,
  help
})