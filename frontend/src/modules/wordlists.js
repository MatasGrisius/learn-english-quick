import axios from 'axios';
import { textToWords } from '../utils/wordsConverter';

export const FETCH_WORDLISTS_RECEIVED = 'wordlists/FETCH_WORDLISTS_RECEIVED'
export const FETCH_WORDLISTS_STARTED = 'wordlists/FETCH_WORDLISTS_STARTED'
export const FETCH_WORDLISTS_ERROR = 'wordlists/FETCH_WORDLISTS_ERROR'
export const SAVE_WORDLIST_STARTED = 'wordlists/SAVE_WORDLIST_STARTED'
export const SAVE_WORDLIST_RECEIVED = 'wordlists/SAVE_WORDLIST_RECEIVED'
export const DOWNLOAD_WORDLIST_STARTED = 'wordlists/DOWNLOAD_WORDLIST_STARTED'
export const DOWNLOAD_WORDLIST_RECEIVED = 'wordlists/DOWNLOAD_WORDLIST_RECEIVED'
export const DOWNLOAD_WORDLIST_ERROR = 'wordlists/DOWNLOAD_WORDLIST_ERROR'

axios.defaults.baseURL = 'http://localhost:5000';

const initialState = {
  loadedWordlist: false,
  loadedWordlists: false,
  wordlists: [],
  error: false,
  wordlist: {},
}

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_WORDLISTS_RECEIVED:
      return {
        loadedWordlists: true,
        wordlists: [
          ...action.wordlists.map(wl => ({
              ...wl,
              words: textToWords(wl.words)
          }))
        ],
        error: false
      }
    case DOWNLOAD_WORDLIST_ERROR:
      return {
        loadedWordlist: true,
        wordlists: [],
        error: true
      }
    case DOWNLOAD_WORDLIST_RECEIVED:
      return {
        loadedWordlist: true,
        wordlist: action.payload,
        error: false
      }
    case DOWNLOAD_WORDLIST_STARTED:
      return {
        loadedWordlist: false
      }
    case FETCH_WORDLISTS_STARTED: {
      return {
        loadedWordlists: false,
        error: false
      }
    }
    case FETCH_WORDLISTS_ERROR: {
      return {
        loadedWordlists: true,
        error: true,
      }
    }
    default:
      return state
  }
}

export const fetchWordlists = () => {
  return dispatch => {
    dispatch({
      type: FETCH_WORDLISTS_STARTED
    })
    axios.get('api/words')
      .then( (res) => {
        dispatch({
          type: FETCH_WORDLISTS_RECEIVED,
          wordlists: res.data
        });
      })
      .catch( (err) => {
        dispatch({
          type: FETCH_WORDLISTS_ERROR
        })
      });
  }
}

export const saveWordlist = (wordlistObject) => {
  return dispatch => {
    dispatch({
      type: SAVE_WORDLIST_STARTED
    })
    axios.post('api/words', {
        ...wordlistObject
      })
      .then( (res) => {
        dispatch({
          type: SAVE_WORDLIST_RECEIVED
        });
      });
  }
}

export const downloadWordlistbyUrl = (url) => {
  return dispatch => {
    dispatch({
      type: DOWNLOAD_WORDLIST_STARTED
    })
    axios.get('api/words/' + url)
      .then( (res) => {
        dispatch({
          type: DOWNLOAD_WORDLIST_RECEIVED,
          payload: res.data
        })
      })
      .catch( (err) => {
        dispatch({
          type: DOWNLOAD_WORDLIST_ERROR
        })
      })
  }
}